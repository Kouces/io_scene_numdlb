-- Super Smash Bros. Ultimate model importer by Random Talking Bush, with help from Ploaj.

-- Changelog:
-- December 26th, 2018
-- Fixed models with 0 rigging influences not being bound to anything.
-- Fixed models with invalid bones causing the script to error.

rollout ModelImporter "Smash Ultimate Model Importer" width:330 height:205
(
	button btnImport "Import *.NUMDLB" pos:[7,8] width:316 height:50
	groupBox OptionsBox "Options" pos:[7,58] width:316 height:40
	checkbox tglVertColors "Enable vertex colors?" pos:[17,75] checked: true
	checkbox tglDebug "Print debug information?" pos:[170,75] checked: false
	label lblCred "This script was written by Random Talking Bush (with Ploaj's help) for use with Super Smash Bros. Ultimate. If used, please consider giving thanks to us, Nintendo, or one of the many third-parties in the game. If something doesn't work right, please contact me on The VG Resource (Random Talking Bush), XeNTaX, Twitter or Steam (RandomTBush) so I can fix it." pos:[8,105] width:317 height:80
	label lblUpdate "(Updated 12/26/2018)" pos:[ModelImporter.width-115,ModelImporter.height-19] width:110


	fn convertTo32 input16 = (
		inputAsInt = input16
		sign = bit.get inputAsInt 16
		exponent = (bit.shift (bit.and inputAsInt (bit.hexasint "7C00")) -10) as integer - 16
		fraction = bit.and inputAsInt (bit.hexasint "03FF")
		if sign==true then sign = 1 else sign = 0
		exponentF = exponent + 127
		outputAsFloat = bit.or (bit.or (bit.shift fraction 13) (bit.shift exponentF 23)) (bit.shift sign 31)
		return bit.intasfloat outputasfloat
	)
	fn readHalfFloat fstream = (
		return convertTo32(Readshort fstream)
	)
	fn printDebug pr = (if tglDebug.state do print(pr))

	on btnImport pressed do(
		clearlistener()
		BoneCount = 0
		MODLGrp_array = #()
		Materials_array = #()
		BoneArray = #()
		BoneFixArray = #()
		BoneTrsArray = #()
		BoneParent_array = #()
		BoneName_array = #()
		PolyGrp_array = #()
		WeightGrp_array = #()
		local VertColors = tglVertColors.state
		mdlname = getOpenFileName \ 
		caption:"Smash Ultimate Model" \
		types:"Smash Ultimate Model(*.numdlb)|*.numdlb" \
		historyCategory:"SmUshPresets"
		if MDLName != undefined do(
			md = fopen MDLName "rb"
			p = getFilenamePath MDLName
			struct MODLStruct (MSHGrpName, MSHMatName)
			
			fseek md 0x10 #seek_set
			MODLCheck = readlong md
			if MODLCheck == 0x4D4F444C do(
				MODLVerA = readshort md #unsigned
				MODLVerB = readshort md #unsigned
				MODLNameOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
				SKTNameOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
				MATNameOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
				fseek md 0x10 #seek_cur
				MSHNameOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
				MSHDatOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
				MSHDatCount = readlong md
				fseek md SKTNameOff #seek_set
				SKTName = (p + readstring md)
				fseek md MATNameOff #seek_set
				MATNameStrLen = readlong md; fseek md 0x04 #seek_cur
				MATName = (p + readstring md)
				fseek md MSHNameOff #seek_set
				MSHName = (p + readstring md); fseek md 0x04 #seek_cur
				fseek md MSHDatOff #seek_set
				for g = 1 to MSHDatCount do(
					MSHGrpNameOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
					MSHUnkNameOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
					MSHMatNameOff = (ftell md) + readlong md; fseek md 0x04 #seek_cur
					MSHRet = (ftell md)
					fseek md MSHGrpNameOff #seek_set
					MSHGrpName = readstring md
					fseek md MSHMatNameOff #seek_set
					MSHMatName = readstring md
					append MODLGrp_array (MODLStruct MSHGrpName:MSHGrpName MSHMatName:MSHMatName)
					fseek md MSHRet #seek_set
				)
				printDebug(MODLGrp_array)
				fclose md
			)

			if MATName != undefined do(
				mt = fopen MATName "rb"
				struct MatStruct (MatName, MatColName, MatCol2Name, MatBakeName, MatNorName, MatEmiName, MatEmi2Name, MatPrmName, MatEnvName)
				
				fseek mt 0x10 #seek_set
				MATCheck = readlong mt
				if MATCheck == 0x4D41544C do(
					MATVerA = readshort mt #unsigned
					MATVerB = readshort mt #unsigned
					MATHeadOff = (ftell mt) + readlong mt; fseek mt 0x04 #seek_cur
					MATCount = readlong mt; fseek mt 0x04 #seek_cur
					fseek mt MATHeadOff #seek_set
					for m = 1 to MATCount do(
						MatColName = ""
						MatCol2Name = ""
						MatBakeName = ""
						MatNorName = ""
						MatEmiName = ""
						MatEmi2Name = ""
						MatPrmName = ""
						MatEnvName = ""
						MATNameOff = (ftell mt) + readlong mt; fseek mt 0x04 #seek_cur
						MATParamGrpOff = (ftell mt) + readlong mt; fseek mt 0x04 #seek_cur
						MATParamGrpCount = readlong mt; fseek mt 0x04 #seek_cur
						MATShdrNameOff = (ftell mt) + readlong mt; fseek mt 0x04 #seek_cur
						MATRet = (ftell mt)
						fseek mt MATNameOff #seek_set
						MatName = readstring mt
						print ("Textures for " + MatName as string + ":")
						fseek mt MATParamGrpOff #seek_set
						for p = 1 to MATParamGrpCount do(
							MatParamID = readlong mt; fseek mt 0x04 #seek_cur
							MatParamOff = (ftell mt) + readlong mt; fseek mt 0x04 #seek_cur
							MatParamType = readlong mt; fseek mt 0x04 #seek_cur
							MatParamRet = (ftell mt)
							if MatParamType == 0x0B do(
								fseek mt (MatParamOff + 0x08) #seek_set
								TexName = readstring mt
								print ("(" + bit.intAsHex(MatParamID) as string + ") for " + TexName as string)
								case MatParamID of(
									default:(printDebug("Unknown type (" + bit.intAsHex(MatParamID) as string + ") for " + TexName as string))
									0x5C:(MatColName = TexName)
									0x5D:(MatCol2Name = TexName)
									0x5F:(MatBakeName = TexName)
									0x60:(MatNorName = TexName)
									0x61:(MatEmiName = TexName; if MatColName == "" do(MatColName = TexName))
									0x62:(MatPrmName = TexName)
									0x63:(MatEnvName = TexName)
									0x65:(MatBakeName = TexName)
									0x66:(MatColName = TexName)
									0x67:(MatCol2Name = TexName)
									0x6A:(MatEmi2Name = TexName; if MatCol2Name == "" do(MatCol2Name = TexName))
									0x133:() -- "noise_for_warp"
								)
								fseek mt MatParamRet #seek_set
							)
						)
						print "-----"
						append Materials_array (MatStruct MatName:MatName MatColName:MatColName MatCol2Name:MatCol2Name MatBakeName:MatBakeName MatNorName:MatNorName MatEmiName:MatEmiName MatEmi2Name:MatEmi2Name MatPrmName:MatPrmName MatEnvName:MatEnvName)
						fseek mt MATRet #seek_set
					)
				)
					multimat = MultiMaterial()
					multimat.name = "SSBUMesh"
					multimat.numsubs = MATCount
					for m = 1 to MATCount do(
					mat = multimat.materialList[m]
					mat.name = Materials_array[m].MatName
					mat.showinviewport = true
					mat.twosided = false
					tm = Bitmaptexture filename:(p + (Materials_array[m].MatColName as string) + ".png")
					tm.alphasource = 0
					mat.diffuseMap = tm
					mat.opacityMap = tm
					mat.opacityMap.monoOutput = 1
					)
				fclose mt
				printDebug(Materials_array)
			)

			if SKTName != undefined do(
				b = fopen SKTName "rb"
				fseek b 0x10 #seek_set
				BoneCheck = readlong b
				if BoneCheck == 0x534B454C do(
					fseek b 0x18 #seek_set
					BoneOffset = (ftell b) + readlong b; fseek b 0x04 #seek_cur
					BoneCount = readlong b; fseek b 0x04 #seek_cur
					BoneMatrOffset = (ftell b) + readlong b; fseek b 0x04 #seek_cur
					BoneMatrCount = readlong b; fseek b 0x04 #seek_cur
					BoneInvMatrOffset = (ftell b) + readlong b; fseek b 0x04 #seek_cur
					BoneInvMatrCount = readlong b; fseek b 0x04 #seek_cur
					BoneRelMatrOffset = (ftell b) + readlong b; fseek b 0x04 #seek_cur
					BoneRelMatrCount = readlong b; fseek b 0x04 #seek_cur
					BoneRelMatrInvOffset = (ftell b) + readlong b; fseek b 0x04 #seek_cur
					BoneRelMatrInvCount = readlong b; fseek b 0x04 #seek_cur
					fseek b BoneOffset #seek_set

					for c = 1 to BoneCount do(
						BoneNameOffset = (ftell b) + readlong b; fseek b 0x04 #seek_cur
						BoneRet = (ftell b)
						fseek b BoneNameOffset #seek_set
						BoneName = readstring b
						fseek b BoneRet #seek_set
						BoneID = readshort b
						BoneParent = readshort b + 1
						BoneUnk = readlong b
						append BoneParent_array BoneParent
						append BoneName_array BoneName
					)
					fseek b BoneMatrOffset #seek_set

					for c = 1 to BoneCount do(
						m11 = readfloat b; m12 = readfloat b; m13 = readfloat b; m14 = readfloat b
						m21 = readfloat b; m22 = readfloat b; m23 = readfloat b; m24 = readfloat b
						m31 = readfloat b; m32 = readfloat b; m33 = readfloat b; m34 = readfloat b
						m41 = readfloat b; m42 = readfloat b; m43 = readfloat b; m44 = readfloat b
						tfm = matrix3 [m11,m12,m13] [m21,m22,m23] [m31,m32,m33] [m41,m42,m43]
						newBone = bonesys.createbone   \
							tfm.row4   \
							(tfm.row4 + 0.01 * (normalize tfm.row1)) \
							(normalize tfm.row3)
							BoneName = BoneName_array[c]
							BoneParent = BoneParent_array[c]
							newBone.name = BoneName
							newBone.width  = 0.01
							newBone.height = 0.01
							newBone.transform = tfm
							newBone.setBoneEnable false 0
							newBone.wirecolor = yellow
							newBone.showlinks = true
							newBone.pos.controller      = TCB_position ()
							newBone.rotation.controller = TCB_rotation ()
							if (BoneParent != 0) then
							newBone.parent = BoneArray[(BoneParent)]
							if BoneParent > c do(append BoneFixArray c) -- This thing again?
							append BoneArray newBone
							append BoneTrsArray newBone.transform
					)
					for x = 1 to BoneFixArray.count do(
						select BoneArray[BoneFixArray[x]]
						$.parent = BoneArray[BoneParent_array[BoneFixArray[x]]]
					)
				)
				fclose b
			)

			if MSHName != undefined do(
				f = fopen MSHName "rb"
				st = timestamp()

				struct weight_data (boneids, weights)
				struct PolyGrpStruct (VisGrpName, SingleBindName, FacepointCount, FacepointStart, FaceLongBit, VertCount, VertStart, VertStride, UVStart, UVStride, BuffParamStart, BuffParamCount)
				struct WeightGrpStruct (GrpName, SubGroupNum, WeightInfMax, WeightFlag2, WeightFlag3, WeightFlag4, RigInfOffset, RigInfCount)

				fseek f 0x10 #seek_set
				MSHCheck = readlong f
				if MSHCheck == 0x4D455348 do(
					fseek f 0x88 #seek_set
					PolyGrpInfOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					PolyGrpCount = readlong f; fseek f 0x04 #seek_cur
					UnkOffset1 = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					UnkCount1 = readlong f; fseek f 0x04 #seek_cur
					FaceBuffSizeB = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					VertBuffOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					UnkCount2 = readlong f; fseek f 0x04 #seek_cur
					FaceBuffOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					FaceBuffSize = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					WeightBuffOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					WeightCount = readlong f; fseek f 0x04 #seek_cur

					fseek f PolyGrpInfOffset #seek_set
					for g = 1 to PolyGrpCount do(
						VisGrpNameOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
						fseek f 0x04 #seek_cur
						Unk1 = readlong f
						SingleBindNameOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
						VertCount = readlong f
						FacepointCount = readlong f
						Unk2 = readlong f -- Always 3?
						VertStart = readlong f
						UVStart = readlong f
						UnkOff1 = readlong f
						Unk3 = readlong f -- Always 0?
						VertStride = readlong f
						UVStride = readlong f
						Unk4 = readlong f -- Either 0 or 32
						Unk5 = readlong f -- Always 0
						FacepointStart = readlong f
						Unk6 = readlong f -- Always 4
						FaceLongBit = readlong f -- Either 0 or 1
						Unk8 = readlong f -- Either 0 or 1
						SortPriority = readlong f
						Unk9 = readlong f -- 0, 1, 256 or 257
						fseek f 0x64 #seek_cur -- A bunch of unknown float values.
						BuffParamStart = (ftell f) + readlong f; fseek f 0x04 #seek_cur
						BuffParamCount = readlong f
						Unk10 = readlong f -- Always 0
						PolyGrpRet = (ftell f)
						fseek f VisGrpNameOffset #seek_set
						VisGrpName = readstring f
						fseek f SingleBindNameOffset #seek_set
						SingleBindName = readstring f
						append PolyGrp_array (PolyGrpStruct VisGrpName:VisGrpName SingleBindName:SingleBindName FacepointCount:FacepointCount FacepointStart:FacepointStart FaceLongBit:FaceLongBit VertCount:VertCount VertStart:VertStart VertStride:VertStride UVStart:UVStart UVStride:UVStride BuffParamStart:BuffParamStart BuffParamCount:BuffParamCount)
						printDebug(VisGrpName as string + " unknowns: 1: " + Unk1 as string + " | Off1: " + UnkOff1 as string + " | 2: " + Unk2 as string + " | 3: " + Unk3 as string + " | 4: " + Unk4 as string + " | 5: " + Unk5 as string + " | 6: " + Unk6 as string + " | LongFace: " + FaceLongBit as string + " | 8: " + Unk8 as string + " | Sort: " + SortPriority as string + " | 9: " + Unk9 as string + " | 10: " + Unk10 as string)
						fseek f PolyGrpRet #seek_set
					)
					printDebug(PolyGrp_array)

					fseek f VertBuffOffset #seek_set
					VertOffStart = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					VertBuffSize = readlong f; fseek f 0x04 #seek_cur
					UVOffStart = (ftell f) + readlong f; fseek f 0x04 #seek_cur
					UVBuffSize = readlong f; fseek f 0x04 #seek_cur
					
					fseek f WeightBuffOffset #seek_set
					
					for b = 1 to WeightCount do(
						GrpNameOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
						SubGroupNum = readlong f; fseek f 0x04 #seek_cur
						WeightInfMax = readbyte f #unsigned; WeightFlag2 = readbyte f #unsigned; WeightFlag3 = readbyte f #unsigned; WeightFlag4 = readbyte f #unsigned; fseek f 0x04 #seek_cur
						RigInfOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
						RigInfCount = readlong f; fseek f 0x04 #seek_cur
						WeightRet = (ftell f)
						fseek f GrpNameOffset #seek_set
						GrpName = readstring f
						append WeightGrp_array (WeightGrpStruct GrpName:GrpName SubGroupNum:SubGroupNum WeightInfMax:WeightInfMax WeightFlag2:WeightFlag2 WeightFlag3:WeightFlag3 WeightFlag4:WeightFlag4 RigInfOffset:RigInfOffset RigInfCount:RigInfCount)
						fseek f WeightRet #seek_set
					)
					printDebug(WeightGrp_array)

					for p = 1 to PolyGrpCount do(
						Vert_array = #()
						Normal_array = #()
						Color_array = #(); Color2_array = #(); Color3_array = #(); Color4_array = #(); Color5_array = #()
						Alpha_array = #(); Alpha2_array = #(); Alpha3_array = #(); Alpha4_array = #(); Alpha5_array = #()
						UV_array = #(); UV2_array = #(); UV3_array = #(); UV4_array = #(); UV5_array = #()
						Face_array = #()
						Weight_array = #()
						SingleBindID = 0

						fseek f PolyGrp_array[p].BuffParamStart #seek_set
						
						PosFmt = 0; NormFmt = 0; TanFmt = 0; ColorCount = 0; UVCount = 0
						
						for v = 1 to PolyGrp_array[p].BuffParamCount do(
							BuffParamType = readlong f
							BuffParamFmt = readlong f + 1 -- Adding one so that "0" counts as "none".
							BuffParamSet = readlong f
							BuffParamOffset = readlong f
							BuffParamLayer = readlong f
							BuffParamUnk1 = readlong f -- always 0?
							BuffParamStrOff1 = (ftell f) + readlong f; fseek f 0x04 #seek_cur
							BuffParamStrOff2 = (ftell f) + readlong f; fseek f 0x04 #seek_cur
							BuffParamUnk2 = readlong f -- always 1?
							BuffParamUnk3 = readlong f -- always 0?
							BuffParamRet = (ftell f)
							fseek f BuffParamStrOff2 #seek_set
							BuffNameOff = (ftell f) + readlong f; fseek f 0x04 #seek_set
							fseek f BuffNameOff #seek_set
							BuffName = readstring f
							case BuffName of(
								default: (throw ("Unknown format!"))
								"Position0":(PosFmt = BuffParamFmt)
								"Normal0":(NormFmt = BuffParamFmt)
								"Tangent0":(TanFmt = BuffParamFmt)
								"map1":(UVCount = UVCount + 1)
								"uvSet":(UVCount = UVCount + 1)
								"uvSet1":(UVCount = UVCount + 1)
								"uvSet2":(UVCount = UVCount + 1)
								"bake1":(UVCount = UVCount + 1)
								"colorSet1":(ColorCount = ColorCount + 1)
								"colorSet2":(ColorCount = ColorCount + 1)
								"colorSet2_1":(ColorCount = ColorCount + 1)
								"colorSet2_2":(ColorCount = ColorCount + 1)
								"colorSet2_3":(ColorCount = ColorCount + 1)
								"colorSet3":(ColorCount = ColorCount + 1)
								"colorSet4":(ColorCount = ColorCount + 1)
								"colorSet5":(ColorCount = ColorCount + 1)
								"colorSet6":(ColorCount = ColorCount + 1)
								"colorSet7":(ColorCount = ColorCount + 1)
							)
							fseek f BuffParamRet #seek_set
						)

						fseek f (VertOffStart + PolyGrp_array[p].VertStart) #seek_set

						printDebug("Vert start: " + (ftell f as string))
						for v = 1 to PolyGrp_array[p].VertCount do(
							case PosFmt of(
								default: ("Unknown position format!")
								1:(
									vx = readfloat f
									vy = readfloat f
									vz = readfloat f
									append Vert_array [vx,vy,vz]
								)
							)
							case NormFmt of(
								default: ("Unknown normals format!")
								6:(
									nx = readhalffloat f * 2 
									ny = readhalffloat f * 2
									nz = readhalffloat f * 2
									nq = readhalffloat f * 2
									append Normal_array [nx,ny,nz]
								)
							)
							case TanFmt of(
								default: ("Unknown tangents format!")
								6:(
									tanx = readhalffloat f * 2
									tany = readhalffloat f * 2
									tanz = readhalffloat f * 2
									tanq = readhalffloat f * 2
								)
							)
						)
						printDebug("Vert end: " + (ftell f as string))

						fseek f (UVOffStart + PolyGrp_array[p].UVStart) #seek_set

						printDebug("UV start: " + (ftell f as string))
						for v = 1 to PolyGrp_array[p].VertCount do(
							case UVCount of(
								default: (throw ("More than 5 UV sets, crashing gracefully."))
								0:(
									append UV_array [0,0,0]
								)
								1:(
									tu = (readhalffloat f * 2)
									tv = ((readhalffloat f * 2) * -1) + 1
									append UV_array [tu,tv,0]
								)
								2:(
									tu = (readhalffloat f * 2)
									tv = ((readhalffloat f * 2) * -1) + 1
									tu2 = (readhalffloat f * 2)
									tv2 = ((readhalffloat f * 2) * -1) + 1
									append UV_array [tu,tv,0]
									append UV2_array [tu2,tv2,0]
								)
								3:(
									tu = (readhalffloat f * 2)
									tv = ((readhalffloat f * 2) * -1) + 1
									tu2 = (readhalffloat f * 2)
									tv2 = ((readhalffloat f * 2) * -1) + 1
									tu3 = (readhalffloat f * 2)
									tv3 = ((readhalffloat f * 2) * -1) + 1
									append UV_array [tu,tv,0]
									append UV2_array [tu2,tv2,0]
									append UV3_array [tu3,tv3,0]
								)
								4:(
									tu = (readhalffloat f * 2)
									tv = ((readhalffloat f * 2) * -1) + 1
									tu2 = (readhalffloat f * 2)
									tv2 = ((readhalffloat f * 2) * -1) + 1
									tu3 = (readhalffloat f * 2)
									tv3 = ((readhalffloat f * 2) * -1) + 1
									tu4 = (readhalffloat f * 2)
									tv4 = ((readhalffloat f * 2) * -1) + 1
									append UV_array [tu,tv,0]
									append UV2_array [tu2,tv2,0]
									append UV3_array [tu3,tv3,0]
									append UV4_array [tu4,tv4,0]
								)
								5:(
									tu = (readhalffloat f * 2)
									tv = ((readhalffloat f * 2) * -1) + 1
									tu2 = (readhalffloat f * 2)
									tv2 = ((readhalffloat f * 2) * -1) + 1
									tu3 = (readhalffloat f * 2)
									tv3 = ((readhalffloat f * 2) * -1) + 1
									tu4 = (readhalffloat f * 2)
									tv4 = ((readhalffloat f * 2) * -1) + 1
									tu5 = (readhalffloat f * 2)
									tv5 = ((readhalffloat f * 2) * -1) + 1
									append UV_array [tu,tv,0]
									append UV2_array [tu2,tv2,0]
									append UV3_array [tu3,tv3,0]
									append UV4_array [tu4,tv4,0]
									append UV5_array [tu5,tv5,0]
								)
							)
							case ColorCount of(
								default: (throw ("More than 5 color sets, crashing gracefully."))
								0:(
									append Color_array [128,128,128]
									append Alpha_array 1
								)
								1:(
									colorr = readbyte f #unsigned
									colorg = readbyte f #unsigned
									colorb = readbyte f #unsigned
									colora = (readbyte f #unsigned as float) / 128
									append Color_array [colorr,colorg,colorb]; append Alpha_array colora
								)
								2:(
									colorr = readbyte f #unsigned
									colorg = readbyte f #unsigned
									colorb = readbyte f #unsigned
									colora = (readbyte f #unsigned as float) / 128
									colorr2 = readbyte f #unsigned
									colorg2 = readbyte f #unsigned
									colorb2 = readbyte f #unsigned
									colora2 = (readbyte f #unsigned as float) / 128
									append Color_array [colorr,colorg,colorb]; append Alpha_array colora
									append Color2_array [colorr2,colorg2,colorb2]; append Alpha2_array colora2
								)
								3:(
									colorr = readbyte f #unsigned
									colorg = readbyte f #unsigned
									colorb = readbyte f #unsigned
									colora = (readbyte f #unsigned as float) / 128
									colorr2 = readbyte f #unsigned
									colorg2 = readbyte f #unsigned
									colorb2 = readbyte f #unsigned
									colora2 = (readbyte f #unsigned as float) / 128
									colorr3 = readbyte f #unsigned
									colorg3 = readbyte f #unsigned
									colorb3 = readbyte f #unsigned
									colora3 = (readbyte f #unsigned as float) / 128
									append Color_array [colorr,colorg,colorb]; append Alpha_array colora
									append Color2_array [colorr2,colorg2,colorb2]; append Alpha2_array colora2
									append Color3_array [colorr3,colorg3,colorb3]; append Alpha3_array colora3
								)
								4:(
									colorr = readbyte f #unsigned
									colorg = readbyte f #unsigned
									colorb = readbyte f #unsigned
									colora = (readbyte f #unsigned as float) / 128
									colorr2 = readbyte f #unsigned
									colorg2 = readbyte f #unsigned
									colorb2 = readbyte f #unsigned
									colora2 = (readbyte f #unsigned as float) / 128
									colorr3 = readbyte f #unsigned
									colorg3 = readbyte f #unsigned
									colorb3 = readbyte f #unsigned
									colora3 = (readbyte f #unsigned as float) / 128
									colorr4 = readbyte f #unsigned
									colorg4 = readbyte f #unsigned
									colorb4 = readbyte f #unsigned
									colora4 = (readbyte f #unsigned as float) / 128
									append Color_array [colorr,colorg,colorb]; append Alpha_array colora
									append Color2_array [colorr2,colorg2,colorb2]; append Alpha2_array colora2
									append Color3_array [colorr3,colorg3,colorb3]; append Alpha3_array colora3
									append Color4_array [colorr4,colorg4,colorb4]; append Alpha4_array colora4
								)
								5:(
									colorr = readbyte f #unsigned
									colorg = readbyte f #unsigned
									colorb = readbyte f #unsigned
									colora = (readbyte f #unsigned as float) / 128
									colorr2 = readbyte f #unsigned
									colorg2 = readbyte f #unsigned
									colorb2 = readbyte f #unsigned
									colora2 = (readbyte f #unsigned as float) / 128
									colorr3 = readbyte f #unsigned
									colorg3 = readbyte f #unsigned
									colorb3 = readbyte f #unsigned
									colora3 = (readbyte f #unsigned as float) / 128
									colorr4 = readbyte f #unsigned
									colorg4 = readbyte f #unsigned
									colorb4 = readbyte f #unsigned
									colora4 = (readbyte f #unsigned as float) / 128
									colorr5 = readbyte f #unsigned
									colorg5 = readbyte f #unsigned
									colorb5 = readbyte f #unsigned
									colora5 = (readbyte f #unsigned as float) / 128
									append Color_array [colorr,colorg,colorb]; append Alpha_array colora
									append Color2_array [colorr2,colorg2,colorb2]; append Alpha2_array colora2
									append Color3_array [colorr3,colorg3,colorb3]; append Alpha3_array colora3
									append Color4_array [colorr4,colorg4,colorb4]; append Alpha4_array colora4
									append Color5_array [colorr5,colorg5,colorb5]; append Alpha5_array colora5
								)
							)
						)
						printDebug("UV end: " + (ftell f as string))

						fseek f (FaceBuffOffset + PolyGrp_array[p].FacepointStart) #seek_set
						printDebug("Face start: " + (ftell f as string))
						for fc = 1 to (PolyGrp_array[p].FacepointCount / 3) do(
							case PolyGrp_array[p].FaceLongBit of(
								default:(throw("Unknown face bit value!"))
								0:(
										fa = readshort f #unsigned + 1
										fb = readshort f #unsigned + 1
										fc = readshort f #unsigned + 1
										append Face_array [fa,fb,fc]
								)
								1:(
										fa = readlong f #unsigned + 1
										fb = readlong f #unsigned + 1
										fc = readlong f #unsigned + 1
										append Face_array [fa,fb,fc]
								)
							)
						)
						printDebug("Face end: " + (ftell f as string))

						if PolyGrp_array[p].SingleBindName != "" then (
							for b = 1 to BoneArray.count do(
								if PolyGrp_array[p].SingleBindName == BoneArray[b].name do(
									SingleBindID = b
								)
							)
							for b = 1 to Vert_array.count do(
								append Weight_array (weight_data boneids:#(SingleBindID) weights:#(1.0))
							)
						) else (
							for b = 1 to Vert_array.count do(
								append Weight_array (weight_data boneids:#() weights:#())
							)
							RigSet = 1
							for b = 1 to WeightGrp_array.count do(
								if PolyGrp_array[p].VisGrpName == WeightGrp_array[b].GrpName do(
									RigSet = b
									WeightGrp_array[b].GrpName = "" -- Dumb fix due to shared group names but split entries, prevents crashing.
									exit
								)
							)

							fseek f WeightGrp_array[RigSet].RigInfOffset #seek_set
							printDebug("Rig info start: " + (ftell f) as string)

							if WeightGrp_array[RigSet].RigInfCount != 0 then (
								for x = 1 to WeightGrp_array[RigSet].RigInfCount do(
									RigBoneNameOffset = (ftell f) + readlong f; fseek f 0x04 #seek_cur
									RigBuffStart = (ftell f) + readlong f; fseek f 0x04 #seek_cur
									RigBuffSize = readlong f; fseek f 0x04 #seek_cur
									RigRet = (ftell f)
									fseek f RigBoneNameOffset #seek_set
									RigBoneName = readstring f
									fseek f RigBuffStart #seek_set
									RigBoneID = 0
									for b = 1 to BoneArray.count do(
										if RigBoneName == BoneArray[b].name do(
											RigBoneID = b
										)
									)
									if RigBoneID == 0 do(
										print (RigBoneName + " doesn't exist on " + PolyGrp_array[p].VisGrpName + "! Transferring rigging to " + BoneArray[1].name + ".")
										RigBoneID = 1
									)
									for y = 1 to (RigBuffSize / 0x06) do(
										RigVertID = readshort f + 1
										RigValue = readfloat f
										append Weight_array[RigVertID].boneids RigBoneID
										append Weight_array[RigVertID].weights RigValue
									)
									fseek f RigRet #seek_set
								) 
							) else (
								print (PolyGrp_array[p].VisGrpName + " has no influences! Treating as a root singlebind instead.")
								Weight_array = #()
								for b = 1 to Vert_array.count do(
									append Weight_array (weight_data boneids:#(1) weights:#(1.0))
								)
							)
						)

						MatID = 1
						for b = 1 to Materials_array.count do(
							if MODLGrp_array[p].MSHMatName == Materials_array[b].MatName do(
								MatID = b
								exit
							)
						)
						msh = mesh vertices:Vert_array faces:Face_array
						msh.numTVerts = Vert_array.count
						if PolyGrp_array[p].SingleBindName != "" do(
							SingleBindID = 1
							for b = 1 to BoneName_array.count do(
								if PolyGrp_array[p].SingleBindName == BoneName_array[b] do(
									SingleBindID = b
									exit
								)
							)
							msh.transform = BoneTrsArray[SingleBindID]
						)
						if VertColors == true then (
							setNumCPVVerts msh msh.numTVerts
							setCVertMode msh true
							setShadeCVerts msh true
						)
						defaultVCFaces msh
						buildTVFaces msh
						msh.name = (PolyGrp_array[p].VisGrpName as string)
						msh.material = multimat
						for j = 1 to UV_array.count do setTVert msh j UV_array[j]
						if UV2_array.count > 0 do(
							meshop.setNumMaps msh 3 keep:true
							for i = 1 to UV2_array.count do (
								meshop.setMapVert msh 2 i UV2_array[i]
							)
						)
						if UV3_array.count > 0 do(
							meshop.setNumMaps msh 4 keep:true
							for i = 1 to UV3_array.count do (
								meshop.setMapVert msh 3 i UV3_array[i]
							)
						)
						if UV4_array.count > 0 do(
							meshop.setNumMaps msh 5 keep:true
							for i = 1 to UV4_array.count do (
								meshop.setMapVert msh 4 i UV4_array[i]
							)
						)
						if UV5_array.count > 0 do(
							meshop.setNumMaps msh 6 keep:true
							for i = 1 to UV5_array.count do (
								meshop.setMapVert msh 5 i UV5_array[i]
							)
						)
						for j = 1 to Face_array.count do (
							setTVFace msh j Face_array[j]
							setFaceMatID msh j MatID
						)
						if VertColors == true do(
							for j = 1 to Color_array.count do setvertcolor msh j Color_array[j]
							for j = 1 to Alpha_array.count do(meshop.setVertAlpha msh -2 j Alpha_array[j])
						)
						for j = 1 to msh.numfaces do setFaceSmoothGroup msh j 1
						max modify mode
						select msh
						
						addmodifier msh (Edit_Normals ()) ui:off
						msh.Edit_Normals.MakeExplicit selection:#{1..Normal_array.count}
						EN_convertVS = msh.Edit_Normals.ConvertVertexSelection
						EN_setNormal = msh.Edit_Normals.SetNormal
						normID = #{}

						for v = 1 to Normal_array.count do(
							free normID
							EN_convertVS #{v} &normID
							for id in normID do EN_setNormal id Normal_array[v]
						)

						if BoneCount > 0 do(
							skinMod = skin ()
							boneIDMap = #()
							addModifier msh skinMod
							msh.Skin.weightAllVertices = false
							for i = 1 to BoneCount do
							(
								 maxbone = getnodebyname BoneArray[i].name
								 if i != BoneCount then
									skinOps.addBone skinMod maxbone 0
								 else
									skinOps.addBone skinMod maxbone 1
							)

							local numSkinBones = skinOps.GetNumberBones skinMod
							for i = 1 to numSkinBones do
							(
								local boneName = skinOps.GetBoneName skinMod i 0
								for j = 1 to BoneCount do
								(
									if boneName == BoneArray[j].Name then
									(
										boneIDMap[j] = i
										j = BoneCount + 1
									)
								)
							) -- This fixes bone ordering in 3DS Max 2012. Thanks to sunnydavis for the fix!

							modPanel.setCurrentObject skinMod

							-- These fix broken rigging for 3DS Max 2015 and above.
							for i = 1 to Vert_array.count do(
								skinOps.SetVertexWeights skinMod i 1 1
								skinOps.unnormalizeVertex skinMod i true 
								skinOps.SetVertexWeights skinMod i 1 0
								skinOps.unnormalizeVertex skinMod i false
							)
							skinOps.RemoveZeroWeights skinMod

							for i = 1 to Weight_array.count do (
								w = Weight_array[i]
								bi = #() --bone index array
								wv = #() --weight value array
								
								for j = 1 to w.boneids.count do
								(
									boneid = w.boneids[j]
									weight = w.weights[j]
									append bi boneIDMap[boneid]
									append wv weight
								)
								skinOps.ReplaceVertexWeights skinMod i bi wv
							)

						)
					)
				)
				fclose f
				Print ("Done! ("+((((timestamp())-st)*0.001)as string)+" Seconds)")
			)

		)
	)

)

CreateDialog ModelImporter
